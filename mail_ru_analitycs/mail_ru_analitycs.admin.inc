<?php

/**
 * @file
 * Admin page callback file for the mail_ru_analitycs module.
 */

function mail_ru_analitycs_page() {
  drupal_add_css(drupal_get_path('module', 'mail_ru_analitycs') . '/mail_ru_analitycs.css');

  $sql = 'SELECT mail FROM {users} WHERE uid != 0';
  $result = db_query($sql);
  $count = 0;
  while ($row = db_fetch_array($result)) {
    $pos = strpos($row['mail'], "@");
    $pos = $pos + 1;
    $host = substr($row['mail'], $pos);
    $hosts[$host][] = $row['mail'];
    $count++;
  }

  $output = '<h3>'. t('Emails statistics') .'</h3>';
  $output .= '<div><p>'. t('Count of all registered users: ') . $count . '</p></div>';
  foreach($hosts as $host => $mails) {
    $out = '';
    $qu = count($mails);
    $percent = $qu / $count * 100;
    $percent = round($percent, 2);
//    $width = $percent * 1.4;
    $width = $percent;
    // если процентов больше 60 могут быть переносы строк
    settype($width, 'int');
    $width_text = ($width) ? $width .'%' : '1px';
    $out .= '<div class="host">';
    $out .= '<div class="host_name">'.$host .' - '. $qu .'</div>';
    $out .= '<div class="host_percent">'.$percent.' %</div>';
    $out .= '<div class="host_diagram" style="width:'.$width.'%">&nbsp;</div>';
    $out .= '</div>';
    $hosts_notsort[$width][$host] = $out;
  }
  asort($hosts_notsort);
  foreach($hosts_notsort as $key => $values) {
    foreach($values as $value) {
      $output .= $value;
    }
  }
  $output .= '<br clear="both">';
  $output .= '<h3>'. t('Registration statistics') .'</h3>';
  // Mail.ru registrations
  $mail_ru_count = db_result(db_query('SELECT COUNT(muid) FROM {mail_ru_users} WHERE muid != 0'));
  $mail_ru_pc = $mail_ru_count / $count * 100;
  $mail_ru_pc = round($mail_ru_pc, 2);
  $output .= sprintf('<div class="host"><div class="host_name">%s </div><div class="host_percent">%s (%s%%)</div></div>', 'mail.ru', $mail_ru_count, $mail_ru_pc);

  // vkontakte.ru registrations
  if(module_exists('vk_openapi')) {
    $vk_count = db_result(db_query('SELECT COUNT(vkuid) FROM {vkontakte_users} WHERE vkuid != 0'));
    $vk_pc = $vk_count / $count * 100;
    $vk_pc = round($vk_pc, 2);
    $output .= sprintf('<div class="host"><div class="host_name">%s </div><div class="host_percent">%s (%s%%)</div></div>', 'vkontakte.ru', $vk_count, $vk_pc);
  }
  // openid registrations
  if(module_exists('openid')) {
    $oi_res = db_query('SELECT authname FROM {authmap} WHERE aid != 0 AND module = "openid"');
    while ($oi_row = db_fetch_array($oi_res)) {
      $res = parse_url($oi_row['authname']);
      $ois[$res['host']][] = $oi_row['authname'];
    }
    if(is_array($ois)) {
      $output .= '<div class="host"><div class="host_name"><b>OpenID</b></div></div>';
      foreach($ois as $host => $oi) {
        $oi_count = count($oi);
        $oi_pc = $oi_count / $count * 100;
        $oi_pc = round($oi_pc, 2);
        $output .= sprintf('<div class="host"><div class="host_name">%s </div><div class="host_percent">%s (%s%%)</div></div>', $host, $oi_count, $oi_pc);

      }

    }

  }

  return $output;
}
