<?php

/**
 * @file
 * Settings of mail_ru_auth
 */

function mail_ru_share_admin_settings() {
  $form = array();
  $form['mail_ru_share_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node types to share'),
  );
  $form['mail_ru_share_types']['mail_ru_share_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types to share in mail.ru'),
    '#options' => node_get_types('names'),
    '#description' => t('Check node types witch you want to share.'),
    '#default_value' => variable_get('mail_ru_share_node_types', array('page' => 'page')),
  );

  $form['mail_ru_share_display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Share button display settings'),
  );
  //TODO display in $links or in $node->content['mail_ru_share']

  $form['mail_ru_share_display']['mail_ru_share_display_page'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display share button on full node pages in links'),
    '#default_value' => variable_get('mail_ru_share_display_page', 1),
  );

  $form['mail_ru_share_display']['mail_ru_share_display_teaser'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display share button on node teasers in links'),
    '#default_value' => variable_get('mail_ru_share_display_teaser', 1),
  );

  $form['mail_ru_share_display']['mail_ru_share_display_alias'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use path alias instead of system path'),
    '#default_value' => variable_get('mail_ru_share_display_alias', 0),
    '#description' => t('Check this checkbox if your aliases do not have russian letters in it.'),
  );
  
  $form['mail_ru_share_display']['mail_ru_share_display_tad'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use teaser as description'),
    '#default_value' => variable_get('mail_ru_share_display_tad', 0),
    '#description' => t('Check this checkbox to use teasers as description on share.'),
  );

  $form['mail_ru_share_display']['mail_ru_share_display_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use node title as a title of sharing'),
    '#default_value' => variable_get('mail_ru_share_display_title', 0),
    '#description' => t('Check this checkbox to use node title as a title of sharing.'),
  );

  $form['mail_ru_share_display']['mail_ru_share_display_picture'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use node author avatar as a picture of sharing'),
    '#default_value' => variable_get('mail_ru_share_display_picture', 0),
    '#description' => t('Check this checkbox to use node author avatar as a picture of sharing.'),
  );

  /*
  $form['mail_ru_share_display']['mail_ru_share_display_position'] = array(
    '#type' => 'radios',
    '#title' => t('Where display share link'),
    '#default_value' => variable_get('mail_ru_share_display_position', 'links'),
    '#options' => array(
      'links' => t('Links of node'),
      'content' => t('Content of node in $node->content[\'mail_ru_share\']'),
    ),
  ); "*/

  $form['mail_ru_share_style'] = array(
    '#type' => 'fieldset',
    '#title' => t('Share button style settings'),
  );
  
  $form['mail_ru_share_style']['mail_ru_share_button_text'] = array(
    '#type' => 'select',
    '#title' => t('Button text'),
    '#default_value' => variable_get('mail_ru_share_button_text', 'Нравится'),
    '#options' => array(
      'Нравится' => 'Нравится',
      'Поделиться' => 'Поделиться',
      'Рекомендую' => 'Рекомендую',
      'Рассказать' => 'Рассказать',
    ),
    '#description' => t('Sorry, this list is limited by mail.ru. No translations.'),
  );
  $form['mail_ru_share_style']['mail_ru_share_button_style'] = array(
    '#type' => 'select',
    '#title' => t('Type of share button'),
    '#default_value' => variable_get('mail_ru_share_button_style','button'),
    '#options' => array(
      //'button_count' => t('Button with counts 120х18'),
      'button' => t('Button with counts 120х18'),
      //'big' => t('Big button with count on top 90х65'),
      'micro' => t('Micro button text+16x16'),
    ),
    '#description' => t('Button styles <a href="!url">description</a>.',
            array('!url' => 'http://api.mail.ru/sites/plugins/share/extended/')),
  );
  $form['mail_ru_share_style']['mail_ru_share_button_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => variable_get('mail_ru_share_button_width', 550),
    '#description' => t('Width of button and text area in pixels'),
  );
  $form['mail_ru_share_style']['mail_ru_share_button_show_text'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display text'),
    '#default_value' => variable_get('mail_ru_share_button_show_text', 0),
  );
  $form['mail_ru_share_style']['mail_ru_share_button_show_faces'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display friends fotos'),
    '#default_value' => variable_get('mail_ru_share_button_show_faces', 0),
  );
  //TODO make update_hook in install file
  //TODO var mail_ru_share_button_style (button_count, big) => button
  return system_settings_form($form);

}
