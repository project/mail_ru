<?php

/**
 * @file
 * Manages tab mail.ru in userprofile
 */

function mail_ru_auth_user_page($account) {
  global $user;
  $output = '';
  drupal_set_title (t('Mail.ru integration'));

  module_load_include('inc', 'mail_ru_auth', 'mail_ru_auth.finalize');
  $muid = _mail_ru_get_id('muid', $account->uid);
  if ($muid) {
    if($account->uid == $user->uid) {
      $output = '<p>'. t('You have integration with Mail.ru.') .'</p>';
    }
    else {
      $output = '<p>'. t('This user have integration with Mail.ru.') .'</p>';
    }
    $output .= '<p>'. t('Here you can change settins of integration or delete it.') .'</p>';
    $state = mail_ru_auth_users_getinfo($muid);
    $output .= drupal_get_form('mail_ru_auth_finalize_user_form', $state, $account);
  }
  elseif ($account->uid == $user->uid) {
    $output = '<div id="mail_ru_auth_login" class="mail_ru_auth_button"></div>';
  }
  else {
    $output = t('This user does not have link to Mail.ru');
  }
  return $output;
}
