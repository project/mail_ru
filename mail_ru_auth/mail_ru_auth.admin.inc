<?php

/**
 * @file
 * Settings of mail_ru_auth
 */

/**
 * Menu callback - admin settings.
 */
function mail_ru_auth_admin_settings() {
  $check_url = 'http://drupal.org';

  $is_curl = function_exists('curl_init');
  $http = drupal_http_request($check_url);
  $is_http = isset($http->code) && ($http->code >= 100) && ($http->code < 600);

  if (!$is_curl) {
    $warning = '<p>' . t('Please enable <a href="@link" target="_blank">CURL library</a>.',
            array('@link' => 'http://php.net/manual/en/book.curl.php')) . '</p>';
  }
  if (!$is_http) {
    $warning = '<p>' . t('Your server not support <a href="@link" target="_blank">drupal_http_request()</a>.<br />
      Request to ' . $check_url . ' fails.',
            array('@link' => 'http://api.drupal.org/api/drupal/includes--common.inc/function/drupal_http_request/6')) . '</p>';
    variable_set('drupal_http_request_fails', TRUE);
  }

  $disabled = (isset($warning)) ? TRUE : FALSE;
  if ($disabled) { variable_del('mail_ru_auth_app_id'); }

  $registered = variable_get('mail_ru_auth_app_id', '') &&
      variable_get('mail_ru_auth_private_key', '') &&
      variable_get('mail_ru_auth_secret_key', '');

  $form['auth'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mail.ru API keys'),
    '#collapsible' => TRUE,
    '#collapsed' => $registered,
  );

  if($warning) {
    $form['auth']['warning'] = array(
      '#type' => 'markup',
      '#value' => '<div class="error">'.$warning.'</div>',
    );
  }

  $form['auth']['register_site'] = array(
    '#type' => 'markup',
    '#value' => '<div class="register_site">'. t('Register your site here: !reg_url first.', array('!reg_url' => l('api.mail.ru/sites/my/add', 'http://api.mail.ru/sites/my/add/'))) .'</div>',
    '#access' => !$registered,
  );

  $form['auth']['mail_ru_auth_app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Application ID'),
    '#description' => t('Enter your Application ID'),
    '#default_value' => variable_get('mail_ru_auth_app_id', ''),
    '#disabled' => $disabled,

  );

  $form['auth']['mail_ru_auth_private_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Private key'),
    '#description' => t('Enter your private key'),
    '#default_value' => variable_get('mail_ru_auth_private_key', ''),
  );

  $form['auth']['mail_ru_auth_secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret key'),
    '#description' => t('Enter your secret key'),
    '#default_value' => variable_get('mail_ru_auth_secret_key', ''),
  );

  $form['login'] = array(
    '#type' => 'fieldset',
    '#title' => t('Login form settings'),
    '#collapsible' => TRUE,
  );
  $form['login']['mail_ru_auth_alter_login_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Mail.ru button on login form'),
    '#default_value' => variable_get('mail_ru_auth_alter_login_form', 1),
    '#description' => t('Add Mail.ru button to login form'),
  );
  $form['login']['mail_ru_auth_alter_login_block'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Mail.ru button on login block'),
    '#return_value' => 1,
    '#default_value' => variable_get('mail_ru_auth_alter_login_block', 1),
    '#description' => t('Add Mail.ru button to login block'),
  );
  $form['login']['mail_ru_auth_alter_register_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Mail.ru button on registration form'),
    '#default_value' => variable_get('mail_ru_auth_alter_register_form', 1),
    '#description' => t('Add Mail.ru button to registration form'),
  );
  $form['login']['mail_ru_auth_button_weight'] = array(
    '#type' => 'select',
    '#title' => t('Weight of button'),
    '#default_value' => variable_get('mail_ru_auth_button_weight', 100),
    '#options' => array(
      '-100' => '-100',
      '-10' => '-10',
      '-2' => '-2',
      '-1' => '-1',
      '0' => '0',
      '1' => '1',
      '2' => '2',
      '10' => '10',
      '100' => '100',
    ),
    '#description' => t('Here you can manage position of the button on your login form.'),
  );

  $form['new_user'] = array(
    '#type' => 'fieldset',
    '#title' => t('New user settings'),
    '#collapsible' => TRUE,
  );
  $form['new_user']['mail_ru_auth_username'] = array(
    '#type' => 'select',
    '#title' => t('Default user name.'),
    '#description' => t('Select "Nick" or "Login". "Nick" - is last and first name. "Login" - is login at mail.ru'),
    '#options' => array(
      MAILRU_DEFAULT_LOGIN => t('Login'),
      MAILRU_DEFAULT_NICK => t('Nick'),
    ),
    '#default_value' => variable_get('mail_ru_auth_username', MAILRU_DEFAULT_LOGIN),
  );

  $user_pictures = variable_get('user_pictures', 0);
  if (!function_exists('curl_init')) {
    $user_pictures = FALSE;
    drupal_set_message(t('You can\'t use mail.ru avatars because your PHP don\'t support <a href="@curl">CURL functions</a>.', array('@curl' => url('http://php.net/curl'))), 'error');
  }

  $form['new_user']['mail_ru_auth_avatar'] = array(
    '#type' => 'select',
    '#title' => t('Use mail.ru avatar'),
    '#default_value' => variable_get('mail_ru_auth_avatar', 0),
    '#options' => array(
      '0' => t('Don\'t use'),
      'pic_small' => t('Use small avatar 45x45'),
      'pic' => t('Use medium avatar 90x90'),
      'pic_big' => t('Use big avatar'),
    ),
    '#disabled' => !$user_pictures,
    '#description' => t('You should enable pictures support on <a href="@usp">user settings page</a>.', array('@usp' => url('admin/user/settings'))),
  );

  $q = db_query("SELECT * FROM {role} WHERE rid > 2 ORDER by name");
  $roles = array();
  while ($r = db_fetch_object($q)) {
    $roles[$r->rid] = $r->name;
  }
  $form['new_user']['mail_ru_auth_role_for_new_user'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Role for new mail.ru user'),
    '#default_value' => variable_get('mail_ru_auth_role_for_new_user', array()),
    '#options' => $roles,
    '#description' => (empty($roles) ? t('No additional roles defined. <a href="@url">Add role.</a>', array('@url' => url('admin/user/roles'))) : ''),
  );

  return system_settings_form($form);
}
