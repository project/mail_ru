<?php

/**
  * @file
  * Finalize mail.ru login for new user
  *
  */

function mail_ru_auth_finalize() {
  global $user; $uid = 0;
  $GLOBALS['conf']['cache'] = false;
  if(isset($_COOKIE['mrc'])) {
    $data = array();
    parse_str(urldecode($_COOKIE['mrc']), $data);

    if(isset($data['vid'])) {
      $uid = _mail_ru_get_id('uid', $data['vid']);
    }

    if($uid && $user->uid == 0) {
      // Instant login of user and goto destination
      mail_ru_auth_session_open();
    }

    $state = mail_ru_auth_users_getinfo($data['vid']);

    if($state) {
      if($user->uid) {
        // First login then make corrections to account
        _mail_ru_direct_save($state, $user);
        // return drupal_get_form('mail_ru_auth_finalize_user_form', $state);
      }
      else {
        $uid = db_result(db_query('SELECT uid FROM {users} WHERE mail = "%s"', $state['email']));
        if($uid) {
          $account = user_load($uid);
          // First login then make corrections to account
          _mail_ru_direct_save($state, $account);
          // return drupal_get_form('mail_ru_auth_finalize_user_form', $state, $account);
        }
        else {
          return drupal_get_form('mail_ru_auth_finalize_anon_form', $state);
        }
      }
    }
    else {
      drupal_set_message(t('Error getting data from mail.ru'), 'error');
      return '';
    }
  }
  else {
    drupal_set_message(t('Please, login via mail.ru first.'), 'error');
    return '<div id="mail_ru_auth_login" class="mail_ru_auth_button"></div>';
  }

}

function mail_ru_auth_finalize_user_form(&$form_state, $state, $account = NULL) {
  global $user;
  $have_settings = FALSE;
  if(is_null($account)) {
    $account = $user;
  }
  $uid = _mail_ru_get_id('uid', $state['muid']);
  $mail_ru_auth_avatar = variable_get('mail_ru_auth_avatar', 0);
  $form = array();
  if($mail_ru_auth_avatar) {
    $have_settings = TRUE;
    $form['pic'] = array(
      '#type' => 'fieldset',
      '#title' => t('Avatar'),
      '#weight' => -2,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    //TODO cleanup WTF sprintf(). Use url() or file_url. check for picture.
    //TODO consider default avatar if none is set
    $form['pic']['mark1'] = array(
      '#type' => 'markup',
      '#value' => sprintf('<div class="picture"><img src="%s?%s" /><span style="font-size: 28pt; margin: 0 10px 30px 10px;"> → </span><img src="%s" /></div>', base_path().$account->picture, time(), _mail_ru_get_pic($state)));
    $sync_default = ($account->mailru['sync_avatar'] === 0) ? $account->mailru['sync_avatar'] : 1;
    $form['pic']['sync_avatar'] = array(
    '#type' => 'checkbox',
      '#title' => t('Sync my avatar'),
      '#return_value' => 1,
      '#default_value' => $sync_default,
      '#description' => t("Sync my avatar with mail.ru on every login."),
    );
  }

  if($account->mail != $state['email']) {
    $have_settings = TRUE;
    $form['email'] = array(
      '#type' => 'fieldset',
      '#title' => t('Email'),
      '#weight' => -1,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['email']['mark2'] = array(
      '#type' => 'markup',
      '#value' => '<div class="email">'.t('Your email now is: <b>@email</b>. Do you want to change it to: <b>@new_email</b>?', array('@email'=>$account->mail, '@new_email' => $state['email'])).'</div>',
    );

    $form['email']['change_email'] = array(
    '#type' => 'checkbox',
      '#title' => t('Change my email'),
      '#return_value' => 1,
      '#default_value' => 1,
    );
  }

  $form['avatar'] = array('#type' => 'hidden', '#value' => _mail_ru_get_pic($state));
  $form['new_email'] = array('#type' => 'hidden', '#value' => $state['email']);
  $form['state'] = array('#type' => 'value', '#value' => $state);
  $form['account'] = array('#type' => 'value', '#value' => $account);
  if($have_settings) {
    $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  }
  else {
    $form['submit'] = array('#type' => 'markup', '#value' => '<p>'. t('There is no settings to save') .'</p>');
  }
  if($uid) {
    $prefix = '<div class="del_button"><p>'. t('Delete integration with mail.ru') .'</p>';
    $form['del'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#prefix' => $prefix,
      '#suffix' => '</div>',
    );
  }
  return $form;
}

function mail_ru_auth_finalize_user_form_submit($form, &$form_state) {
  global $user;
  $account      = $form_state['values']['account'];
  $sync_avatar  = $form_state['values']['sync_avatar'];
  $photo        = $form_state['values']['avatar'];
  $change_email = $form_state['values']['change_email'];
  $new_email    = $form_state['values']['new_email'];
  $state        = $form_state['values']['state'];

  if($form_state['clicked_button']['#value'] == t('Delete')) {
    // Delete link to mail.ru
    db_query('DELETE FROM {mail_ru_users} WHERE uid = %d', $account->uid);
    SetCookie("mail_ru_auth", '', 0, '/');
    $array = array(
      'mailru' => array(
        'link' => '',
        'muid' => '',
        'pic_small' => '',
        'pic_big' => '',
        'pic' => '',
        'nick' => '',
        'login' => '',
        'email' => '',
        'sync_avatar' => '',
      ),
    );
    user_save($account, $array);
    watchdog('mail_ru', 'Connection with mail.ru for user %uname deleted', array('%uname' => $account->name));
  }
  else {
  // Save link to mail.ru
  SetCookie("mail_ru_auth", 'logged', 0, '/');
  $array = array(
    'mailru' => array(
      'link' => $state['link'],
      'muid' => $state['muid'],
      'pic_small' => $state['pic_small'],
      'pic_big' => $state['pic_big'],
      'pic' => $state['pic'],
      'nick' => $state['nick'],
      'login' => $state['login'],
      'email' => $state['email'],
      'sync_avatar' => $sync_avatar,
    ),
  );

  if($change_email) {
    $array['mail'] = $new_email;
  }
  if($sync_avatar) {
    if(variable_get('user_pictures', 0)) {
      $avatar = _mail_ru_auth_save_remote_image($photo, $account);
      $array['picture'] = $avatar;
    }
  }

  // Set user roles
  _mail_ru_set_roles($array, $account);
  
  if($user->uid === 0) {
    $user = $account;
    $array['login'] = time();
  }
  user_save($account, $array);


  $muid = _mail_ru_get_id('muid', $account->uid);
  $time = time();
  if($muid) {
    $sql = 'UPDATE {mail_ru_users} SET timestamp = %d WHERE muid = %s';
    db_query($sql, $time, $muid);
  }
  else {
    $sql = 'INSERT INTO {mail_ru_users} (uid, muid, timestamp) VALUES (%d, %s, %d)';
    db_query($sql, $account->uid, $state['muid'], $time);
  }

  if(arg(0) == 'mail_ru_auth') {
    drupal_goto('user/'. $account->uid);
  }

  }
}

function mail_ru_auth_finalize_anon_form(&$form_state, $state, $account = NULL) {
  global $user;
  if(is_null($account)) {
    $account = $user;
  }

  $form = array();
  $form['you_are'] = array(
    '#type' => 'markup',
    '#value' => '<div>'.t('You are:').'</div>',
    '#weight' => -3,
  );

  $form['reg'] = array(
    '#type' => 'fieldset',
    '#title' => t('A new user'),
    '#weight' => -2,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#attributes' => array('class' => 'mail_ru_new_user'),
  );

  $name_type = variable_get('mail_ru_auth_username', MAILRU_DEFAULT_LOGIN);
  $default_name = ($name_type == MAILRU_DEFAULT_NICK) ? $state['nick'] : $state['login'];
  $default_name = _mail_ru_auth_check_name($default_name);

  $form['reg']['new_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#size' => 30,
    '#maxlength' => USERNAME_MAX_LENGTH,
    '#required' => TRUE,
    '#default_value' => $default_name,
  );

  $form['reg']['new_mail'] = array(
    '#type' => 'textfield',
    '#title' => t('My email'),
    '#size' => 30,
    '#default_value' => $state['email'],
    '#required' => TRUE,
    '#description' => t('New user will be created usign given login and email'),
  );

  $form['reg']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create new user'),
    '#prefix' => '<div class="create_new_user">',
    '#suffix' => '</div>',
  );

  $form['log'] = array(
    '#type' => 'fieldset',
    '#title' => t('A registered user'),
    '#weight' => -1,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#attributes' => array('class' => 'mail_ru_reg_user'),
  );
  $form['log']['name'] = array('#type' => 'textfield',
    '#title' => t('Username'),
    '#size' => 30,
    '#maxlength' => USERNAME_MAX_LENGTH,
  );
  $form['log']['pass'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#description' => t('Enter the password that accompanies your username.'),
    '#size' => 30,
  );
  $form['log']['submit'] = array('#type' => 'submit', '#value' => t('Log in'), '#weight' => 2);

  $form['#validate'] = array(
    'mail_ru_auth_name_validate', 'mail_ru_auth_authenticate_validate', 'mail_ru_auth_final_validate'
  );

  $form['avatar'] = array('#type' => 'hidden', '#value' => _mail_ru_get_pic($state));
  $form['state'] = array('#type' => 'value', '#value' => $state);
  $form['account'] = array('#type' => 'value', '#value' => $account);
  $form['br'] = array(
    '#type' => 'markup',
    '#value' => '<br clear="both">',
    '#weight' => 1000,
  );
  return $form;
}

function mail_ru_auth_finalize_anon_form_submit($form, &$form_state) {
  global $user;
  
  $account      = $form_state['values']['account'];
  $new_name     = $form_state['values']['new_name'];
  $mail         = $form_state['values']['new_mail'];
  $sync_avatar  = 1;
  $photo        = $form_state['values']['avatar'];
  $state        = $form_state['values']['state'];

  $array = array(
    'mailru' => array(
      'link' => $state['link'],
      'muid' => $state['muid'],
      'pic_small' => $state['pic_small'],
      'pic_big' => $state['pic_big'],
      'pic' => $state['pic'],
      'nick' => $state['nick'],
      'login' => $state['login'],
      'email' => $state['email'],
      'sync_avatar' => $sync_avatar,
    ),
  );

  if(!empty($form_state['values']['name']) && !empty($form_state['values']['pass'])) {
    $account = $user;
    watchdog('mail_ru', 'User %uname make connection with mail.ru', array('%uname' => $account->name));
  }
  else {
    $array['pass'] = user_password();
    $array['mail'] = $mail;
    $array['name'] = $new_name;
    $array['status'] = 1;
    $account = user_save(NULL, $array);
    watchdog('mail_ru', 'User %uname created via mail.ru', array('%uname' => $account->name));
  }

  if($sync_avatar) {
    if(variable_get('user_pictures', 0)) {
      $avatar = _mail_ru_auth_save_remote_image($photo, $account);
      $array['picture'] = $avatar;
    }
  }

  // Set user roles
  _mail_ru_set_roles($array, $account);
  
  SetCookie("mail_ru_auth", 'logged', 0, '/');
  $array['login'] = time();
  $account = user_save($account, $array);
  $user = $account;

  $muid = _mail_ru_get_id('muid', $account->uid);
  $time = time();
  if($muid) {
    $sql = 'UPDATE {mail_ru_users} SET timestamp = %d WHERE muid = %s';
    db_query($sql, $time, $muid);
  }
  else {
    $sql = 'INSERT INTO {mail_ru_users} (uid, muid, timestamp) VALUES (%d, %s, %d)';
    db_query($sql, $account->uid, $state['muid'], $time);
  }

  if(arg(0) == 'mail_ru_auth') {
    drupal_goto('user/'. $account->uid);
  }
}

function _mail_ru_direct_save($state, $account) {
  $form = array(); $form_state = array();
  $form_state['values']['account'] = $account;
  $form_state['values']['sync_avatar'] = 0;
  $form_state['values']['change_email'] = 0;
  $form_state['values']['state'] = $state;
  $form_state['clicked_button']['#value'] = t('Save');
  mail_ru_auth_finalize_user_form_submit($form, $form_state);
}

/**
 * <array>  $array - array of data for user to save
 * <object> $account - user object
 */
function _mail_ru_set_roles(&$array, $account) {
  $roles = array();
  $uroles = array();
  $q = db_query("SELECT * FROM {role} WHERE rid > 2");
  while($r = db_fetch_object($q)) {
    $roles[$r->rid] = $r->name;
  }
  $rs = variable_get('mail_ru_auth_role_for_new_user', 0);
  if(is_array($rs)) {
    foreach($rs as $rid) {
      if($rid) {
        $uroles[$rid] = $roles[$rid];
      }
    }
    if(is_array($uroles)) {
      if(is_array($account->roles)) {
        $array['roles'] = $account->roles + $uroles;
      }
      else {
        $array['roles'] = $uroles;
      }
    }
  }
}