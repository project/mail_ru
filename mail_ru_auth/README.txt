
Mail.ru - is Russian national email service.
Statistics shows up to 60%-70% user registrations using emails from Mail.ru service

Mail.ru authentication module allow users to enter your site with account on mail.ru

FEATURES
---------
Automatic detection of emails of registered users
Ability to assign multiple roles for new users
Synchronizing the avatar on each user login (optionally for each user)

INSTALL
------------------

1. Download module from drupal.org
2. Copy files to your modules directory. Often its 'sites/all/modules'
3. Go to admin/modules page and enable Mail.ru Auth module
4. Now you have to register your web-site
   Go to http://api.mail.ru/sites/my/add/
   You have to be logged at http://my.mail.ru
   Accept user agreement.
   Enter title of your site and front page url

   After registration you've got ID, Secret key and Private key

   You have to enter these values at the module settings page.

6. Go to admin/settings/mail_ru_auth
   enter your ID, Secret key and Private key
   Login button will be displayed only if you have entered all 3 values.


DEPENDENCIES
------------

cURL http://php.net/manual/en/book.curl.php
drupal_http_request()
JavaScript
Cookies
